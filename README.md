# ts_gra
Przykład projektu opartego o kodowanie w typescript **<https://www.typescriptlang.org/docs/>**

Wykorzystanie nodes i webpack do compilacji ts i budowania paczki 

VSCode jako narzedzie developerskie.

# Przygotowanie środowiska
--------------------------------

### 1. On local machine: 
install Node,js: **<https://nodejs.org/>**  
install vscode: **<https://code.visualstudio.com/download>**

### 2. Clone repository from bitbucket: 
 **git clone https://lleszek@bitbucket.org/ms2018dev/tssample.git**

lub za pomocą SourceTree 
**sourcetree://cloneRepo?cloneUrl=https%3A%2F%2Flleszek%40bitbucket.org%2Fms2018dev%2Ftssample.git&type=bitbucket**

### 3. Uruchom vscode i wskaż otwarcie workspace na folder z projektem

### 4. Poczekaj aż VSCODE zaczyta folder ... poowinna się pojawić prośba o zainstalowanie dodatków 
 - pozwól na zainstalowanie dodatków do VSCODE 😊

### 5. Zainstaluj wymagane pakiety Node
 - w VSCODE menu: Terminal -> new Terminal
 - w otwartym na dole okienku terminala podaj komendę:
    **npm install**
 - poczekaj aż Node wykona robotę 😊   

 **YUPI ! Jesteś gotów do kodowania!**


# Kodowanie
--------------------------------

src - folder do kodowania
dist - folder gdzie są wynikowe buildy 

#### Kompilacja developerska
- jeśli nie masz otwartego otwórz okno Terminala
- podaj komendę: **npm run build-dev**
- jesli nie było błędów w folderze **./dist** znajdziesz wytworzoną gotową paczkę plików wynikowych
- kliknij w index.html i z dolnego paska VSCODE kliknij w **"Go Live"**
- w przeglądarce otworzy się nasza aplikacja: 
- automatycznie odświeżana(!) po wykonaniu build'a ;)

#### Kompilacja produkcyjna
 - jeśli nie masz otwartego otwórz okno Terminala
 - podaj komendę: **npm run build-prod**
 

## Credits
<leszeklezon@gmail.com>