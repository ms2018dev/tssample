'use strict';

import { Keyboard } from './keyboard';
import { fpsLimit, playerSpeed } from './params';

type State = 'normal' | 'blink';

export class Player {
    // normal, blink
    state: State = 'normal';
    lastBlink = 0;
    blinkTime = 200;
    speed: number = 5;
    posx: number;
    posy: number;
    kyb: Keyboard;
    lastFrame: number = 0;
    color: string;

    constructor(kyb: Keyboard, x: number, y: number, color: string) {
        this.posx = Math.floor(x);
        this.posy = Math.floor(y);
        this.color = color;
        this.kyb = kyb;
        this.speed =
            playerSpeed * (Math.random() > 0.5 ? 1 : -1) * Math.random() * 5;
    }

    private setState() {
        let curr = performance.now();
        let diff = curr - this.lastBlink;
        if (diff > 1000 * Math.random() * 125) {
            this.state = 'blink';
            this.lastBlink = performance.now();
        } else if (diff > this.blinkTime) {
            this.state = 'normal';
        }
    }

    refresh(cumulTime: number): number {
        let speed_ = this.speed;
        this.setState();
        while (cumulTime > 1000 / fpsLimit) {
            this.kyb.keysArr.forEach((key) => {
                if (key === 'ArrowRight') this.posx += Math.floor(speed_);
                if (key === 'ArrowLeft') this.posx -= Math.floor(speed_);
                if (key === 'ArrowUp') this.posy -= Math.floor(speed_);
                if (key === 'ArrowDown') this.posy += Math.floor(speed_);
            });
            cumulTime = cumulTime - 1000 / fpsLimit;
        }
        return cumulTime;
    }

    draw(canv: HTMLCanvasElement) {
        this.drawHead(canv);
        this.drawEyes(canv);
    }

    drawHead(canv: HTMLCanvasElement) {
        let ctx: CanvasRenderingContext2D = canv.getContext('2d')!;
        //ctx.save();

        ctx.beginPath();
        ctx.fillStyle = this.color;
        ctx.arc(this.posx + 75, this.posy + 75, 40, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();

        ctx.beginPath();
        ctx.arc(this.posx + 75, this.posy + 90, 10, 0, Math.PI * 0.8, false);

        ctx.stroke();

        //ctx.restore();
    }

    drawEyes(canv: HTMLCanvasElement) {
        let ctx: CanvasRenderingContext2D = canv.getContext('2d')!;

        //ctx.save();
        ctx.fillStyle = 'White';
        let pi = this.state == 'blink' ? Math.PI : Math.PI * 2;
        let contin = this.state == 'blink' ? false : true;
        let startR = this.state == 'blink' ? 1.2 : 0;

        ctx.beginPath();
        ctx.arc(this.posx + 60, this.posy + 60, 5, startR, pi, contin);
        ctx.fill();

        ctx.beginPath();
        ctx.arc(this.posx + 80, this.posy + 60, 5, startR, pi, contin);
        ctx.fill();
    }
} //gracz
