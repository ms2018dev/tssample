export class Log {
    debug: boolean = true;
    write(message: string) {
        if (this.debug) console.log(message);
    }
}
