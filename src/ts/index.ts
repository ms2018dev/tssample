'use strict';
import { Game as Game } from './game';

window.addEventListener('load', () => {
    let canv = document.getElementsByTagName('canvas')[0];

    canv.style.height = window.innerHeight.toString() + 'px';
    canv.style.width = window.outerWidth.toString() + 'px';
    canv.height = window.innerHeight;
    canv.width = window.outerWidth;

    let game = new Game();

    game.start();
    game.cumulTime = 0;
    game.lastFrame = performance.now();
    requestAnimationFrame(game.gameLoop);

    canv.addEventListener('click', () => game.addPlayers());
});
