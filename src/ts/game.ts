'use strict';
import { Player } from './player';
import { Keyboard as Keyboard } from './keyboard';
import { Log } from './log';
import { fpsLimit } from './params';

export class Game {
    debug = true;
    canv: HTMLCanvasElement;
    log_: Log;
    kyb: Keyboard;
    players: Player[];
    lastFrame = 0;
    cumulTime: number = 0;

    constructor() {
        this.canv = document.getElementsByTagName('canvas')[0];
        this.log_ = new Log();
        this.kyb = new Keyboard(this.log_);
        this.players = [];
        //console.log(this);
    }

    addPlayers() {
        for (let i = 0; i < 20; i++) {
            this.players.push(
                new Player(
                    this.kyb,
                    Math.random() * this.canv.width * 0.7,
                    Math.random() * this.canv.height * 0.7,
                    ['yellow', 'red', 'blue', 'green', 'black'][
                        Math.min(i % 5, 5)
                    ]
                )
            );
        }
    }

    start(): void {
        this.log_.debug = this.debug;
        this.kyb.init();
        this.addPlayers();
    }

    gameLoop: (timeelapsed: number) => void = (timeelapsed) => {
        this.cumulTime += timeelapsed - this.lastFrame;

        requestAnimationFrame((timeelaps) => {
            this.gameLoop(timeelaps);
        });

        if (this.cumulTime < 1000 / fpsLimit) return;

        this.lastFrame = timeelapsed;
        let canv_ = this.canv;
        let ctx: CanvasRenderingContext2D = canv_.getContext('2d')!;

        let pom = 0;
        this.players.forEach((player) => {
            pom = player.refresh(this.cumulTime);
        });
        this.cumulTime = pom;

        //console.log(this.cumulTime);

        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, canv_.width, canv_.height);
        ctx.stroke();
        this.players.forEach((player) => {
            player.draw(canv_);
        });
    };
} //gra
