import { Log } from './log';

export class Keyboard {
    keysArr: string[] = [];
    log_: Log;

    constructor(log: Log) {
        this.log_ = log;
    }

    init(): void {
        window.addEventListener('keydown', (e: KeyboardEvent) => {
            this.addKey(e);
        });
        window.addEventListener('keyup', (e: KeyboardEvent) => {
            this.delKey(e);
        });
    }

    addKey(e: KeyboardEvent) {
        this.toogleKey('add', e.code);
    }
    delKey(e: KeyboardEvent) {
        this.toogleKey('del', e.code);
    }

    toogleKey(mode: string, keyCode: string) {
        let findKey = this.keysArr.indexOf(keyCode);
        if (mode == 'add') {
            if (findKey == -1) this.keysArr.push(keyCode);
        } else {
            this.keysArr.splice(findKey, 1);
        }
        //this.log_.write(this.keysArr.toString());
    }
} //klawiatura
